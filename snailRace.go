// https://edabit.com/challenge/DGpxmRkADuZaWHJxZ
// go run snailRace.go 3 5 10 4 7 11

package main 

import (
	"fmt"
	"os"
	"strconv"
)

func main() {

	total := race(os.Args[1], os.Args[6])
	total += race(os.Args[2], os.Args[4])
	total += race(os.Args[3], os.Args[5])

	if total >= 2 {
		fmt.Println("True")
	} else {
		fmt.Println("False")
	}	
}

func race(x, y string) int {

	a,_ := strconv.Atoi(x)
	b,_ := strconv.Atoi(y)

	if a > b {
		return 1
	} else {
		return 0
	}
}