// https://edabit.com/challenge/MhQbon8XzsG3wJHdP

package main

import (
	"flag"
	"fmt"
)

func main() {

	basePtr := flag.Int("base", 1, "the base value")
	productPtr := flag.Int("product", 1, "the end product")
	flag.Parse()

	count := 1

	for {

		*productPtr /= *basePtr

		if *productPtr == 1 {
			break
		}

		count++
	}

	fmt.Println(count)

}